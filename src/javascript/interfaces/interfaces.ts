export interface fighterData {
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
}

export interface fightingPlayer extends fighterData {
  healthLeft?: number;
  isSuperHitAllowed?: boolean;
  superCombination?: Array<string>;
}

export interface keysPressed {
  [index: string]: boolean;
}

export type winnerData = {
  title: string;
  bodyElement: Element;
  onClose(): void;
}

export enum keys {
  KeyA = 'KeyA',
  KeyD = 'KeyD',
  KeyJ = 'KeyJ',
  KeyL = 'KeyL',
  KeyQ = 'KeyQ',
  KeyW = 'KeyW',
  KeyE = 'KeyE',
  KeyU = 'KeyU',
  KeyI = 'KeyI',
  KeyO = 'KeyO',
}