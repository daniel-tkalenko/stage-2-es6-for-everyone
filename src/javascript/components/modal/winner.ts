import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { fighterData, winnerData } from '../../interfaces/interfaces';

export function showWinnerModal(fighter: fighterData): void {
    const winnerImageElement: Element = createFighterImage(fighter);
    const winnerData: winnerData = {
        title: fighter.name + " - winner!",
        bodyElement: winnerImageElement,
        onClose() {
            document.location.reload();
        }
    }
    showModal(winnerData);
}
