import { createElement } from '../helpers/domHelper';
import { fighterData } from '../interfaces/interfaces';

export function createFighterPreview(fighter: fighterData, position: string) : Element {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: Element = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  const fighterImage: Element = createFighterImage(fighter);
  const fighterInfoElement: Element = createElement({
    tagName: 'div',
    className: `fighter-preview___info`,
  });

  fighterInfoElement.innerHTML = `
    <span class="fighter-preview___info-thin">Name</span>: ${fighter.name} <br>
    <span class="fighter-preview___info-thin">Health</span>: ${fighter.health} <br>
    <span class="fighter-preview___info-thin">Attack</span>: ${fighter.attack} <br>
    <span class="fighter-preview___info-thin">Defense</span>: ${fighter.defense} <br>
  `;

  fighterElement.append(fighterImage, fighterInfoElement);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
