import { controls } from '../../constants/controls';
import { fighterData, fightingPlayer, keysPressed, keys } from '../interfaces/interfaces';

export async function fight(firstFighter: fightingPlayer, secondFighter: fightingPlayer): Promise<fighterData> {
  return new Promise((resolve) => {
    firstFighter.healthLeft = firstFighter.health;
    secondFighter.healthLeft = secondFighter.health;
    firstFighter.superCombination = controls.PlayerOneCriticalHitCombination;
    secondFighter.superCombination = controls.PlayerTwoCriticalHitCombination;    
    firstFighter.isSuperHitAllowed = secondFighter.isSuperHitAllowed = true;

    let keysPressed: keysPressed = {};
    const timeToAllowCriticalHit: number = 10000;

    document.addEventListener('keydown', keydownHandler);
    document.addEventListener('keyup', keyupHandler);

    function keydownHandler(event: KeyboardEvent): void {
      keysPressed[event.code] = true;
      userActionsHandler(event.code);
    }

    function keyupHandler(event: KeyboardEvent): void {
      delete keysPressed[event.code];
    }

    function superHitHandler(atacker: fightingPlayer, defender: fightingPlayer): void {
      if (atacker.isSuperHitAllowed && atacker.superCombination.every(key => keysPressed[key])) {
        updateHealth(defender, atacker.attack * 2);
        atacker.isSuperHitAllowed = false;
        setTimeout(() => { atacker.isSuperHitAllowed = true }, timeToAllowCriticalHit);
      }
    }

    function userActionsHandler(key: string): void {
      switch (key) {
        case keys.KeyQ:
        case keys.KeyW:
        case keys.KeyE:
          superHitHandler(firstFighter, secondFighter);
          break;
        case keys.KeyU:
        case keys.KeyI:
        case keys.KeyO:
          superHitHandler(secondFighter, firstFighter);
          break;
        case controls.PlayerOneAttack:
          if (keysPressed[controls.PlayerOneBlock]) break;
          if (keysPressed[controls.PlayerTwoBlock]) {
            updateHealth(secondFighter, getDamage(firstFighter, secondFighter));
          } else {
            updateHealth(secondFighter, getHitPower(firstFighter));
          }
          break;
        case controls.PlayerTwoAttack:
          if (keysPressed[controls.PlayerTwoBlock]) break;
          if (keysPressed[controls.PlayerOneBlock]) {
            updateHealth(firstFighter, getDamage(secondFighter, firstFighter));
          } else {
            updateHealth(firstFighter, getHitPower(secondFighter));
          }
          break;
      }

      renderHealth(firstFighter, secondFighter);
      if (firstFighter.healthLeft <= 0) endFight(secondFighter);
      if (secondFighter.healthLeft <= 0) endFight(firstFighter);
    }

    function updateHealth(defender: fightingPlayer, damage: number): void {
      defender.healthLeft -= damage;
    }

    function renderHealth(firstFighter: fightingPlayer, secondFighter: fightingPlayer): void {
      const leftHealthBar: Element = document.querySelector('#left-fighter-indicator');
      const rightHealthBar: Element = document.querySelector('#right-fighter-indicator');

      const leftFighterHealthLeft: number = firstFighter.healthLeft / firstFighter.health * 100;
      const rightFighterHealthLeft: number = secondFighter.healthLeft / secondFighter.health * 100;

      leftHealthBar.setAttribute("style", `width: ${leftFighterHealthLeft}%`);
      rightHealthBar.setAttribute("style", `width: ${rightFighterHealthLeft}%`);
    }

    function endFight(winner: fightingPlayer): void {
      document.removeEventListener('keydown', keydownHandler);
      document.removeEventListener('keyup', keyupHandler);
      resolve(winner);
    }
  });
}

export function getDamage(attacker: fightingPlayer, defender: fightingPlayer): number {
  const damage: number = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter: fightingPlayer): number {
  const criticalHitChance: number = Math.random() + 1;
  const power: number = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: fightingPlayer): number {
  const dodgeChance: number  = Math.random() + 1;
  const power: number = fighter.defense  * dodgeChance ;
  return power;
}
